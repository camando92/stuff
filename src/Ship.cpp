#include "Ship.h"
#include <cmath>
#include "SDL/SDL.h"
#undef main
#include "Projectile.h"
#include "NukeProjectile.h"
#include "Asteroids.h"
 void Ship::draw (Graphics*g)
 {
     // all of the ships points need to be multiplied by mat and have pos added to them
     //use the current call to line as a template to amek all of the others
     //also peta says that x is forward...  SO LONG MAJICAL BUNNY RABBITS AND RAINBOW UNICORMS... rainicorns... from adventure time... the most m,ajical (and mental) tv show for kids in the world.....

     if (_hitTime/5%2==0)
     {
         glm::mat2 mat=glm::mat2 (cosf (_rotation),-sinf(_rotation),sinf(_rotation),cosf(_rotation));
     g->line (glm::vec2 (0.,-10.)*mat+_pos,glm::vec2 (20.,0.)*mat+_pos,glm::vec4 (1,0,0,1));
     g->line (glm::vec2 (0.,10.)*mat+_pos,glm::vec2 (20.,0.)*mat+_pos,glm::vec4 (1,0,0,1));
     g->line (glm::vec2 (0.,-10.)*mat+_pos,glm::vec2 (10.,1.)*mat+_pos,glm::vec4 (1,0,0,1));
     g->line (glm::vec2 (0.,10.)*mat+_pos,glm::vec2 (10.,1.)*mat+_pos,glm::vec4 (1,0,0,1));
     }
    for (int i=_health;i>0;i--)
    {
        g->line(glm::vec2 (i*4+4.,50.),glm::vec2 (i*4+4.,0.),glm::vec4 (1.,1.,0.,1.));
        g->line(glm::vec2 (i*4+5.,50.),glm::vec2 (i*4+5.,0.),glm::vec4 (1.,1.,0.,1.));
    }



 }



void Ship::tick (bool* keys)
{
    _pos+=_vel;
    _vel*=0.99945;
    if (keys [SDLK_a])
    {
        _rotation+=0.003;
    }
    if (keys [SDLK_d])
    {
        _rotation-=0.003;
    }
    if (keys [SDLK_SPACE])
    {

        if (_cooldown==0)
        {
            new NukeProjectile (_pos,_vel+glm::vec2(cosf(_rotation),sinf (_rotation))*0.25f);
            //glm::vec2 dir=glm::vec2 (cosf(_rotation),sinf (_rotation));
        //_vel-=dir*0.03f;
            _cooldown=100;
        }
    }
    if (keys [SDLK_w])
    {
        glm::vec2 dir=glm::vec2 (cosf(_rotation),sinf (_rotation));
        _vel+=dir*0.0003f;
    }
    if (_pos.x>=512)
    {
        _pos.x-=512.f;
    }
    if (_pos.y>=512)
    {
        _pos.y-=512;
    }
    if (_pos.y<0)
    {
        _pos.y+=512;
    }
    if (_pos.x<0)
    {
        _pos.x+=512;
    }
    _cooldown--;
    if (_cooldown<0)
    {
        _cooldown=0;
    }
    if (_hitTime>0)
    {
        _hitTime--;
    }
    else
    {
         std::list <Asteroids*>& asteroids=Asteroids::get_asteroids();
        std::list <Asteroids*>::iterator i=asteroids.begin();
        for (;i!=asteroids.end();i++)
        {

            Asteroids* a=*i;
            float dist=glm::length(a->getPos()-_pos);
            if (dist<a->getRadius()+10)
            {
                a->doDamage(10.0);
                _health--;
                if (_health>0)
                {
                    _hitTime=2550;
                }
                else
                {
                    printf ("UR DEAD HAHAHAHA U FAILED \n insert initials here... OH WAIT YOU CANT HAAHHAHAH");
                }
            }

        }
    }
}
Ship::Ship()
{
    _health=14;
    _cooldown=0;
    _hitTime=0;
    _vel=glm::vec2 (0.,0.);
    //ctor
    _pos=glm::vec2 (256.,256.);
    _rotation=0;
}

Ship::~Ship()
{
    //dtor
}
