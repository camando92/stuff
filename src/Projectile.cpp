#include "Projectile.h"
#include <list>
#include <cstdio>
#include "Asteroids.h"


std::list<Projectile*> projectiles;
void Projectile::tick_all ()
{
    //printf ("%d\n",projectiles.size());
    std::list<Projectile*>::iterator it=projectiles.begin();
    for (;it!=projectiles.end();)
    {
        Projectile* proj=*it;
        bool dead=(proj)->tick ();
        if (dead)
        {
            it=projectiles.erase(it);
            delete proj;
        }
        else
        {
            it++;
        }


    }
}
void Projectile::draw_all (Graphics* grampa)
{
    std::list<Projectile*>::iterator it=projectiles.begin();
    for (;it!=projectiles.end();it++)
    {
        (*it)->draw (grampa);
    }
}
void Projectile::draw (Graphics* gramma)
{
    gramma->line (glm::vec2 (-1.,-1.)+_pos,glm::vec2 (1.,1.)+_pos,glm::vec4 (0.,1.,0.,1));
    gramma->line (glm::vec2 (-1.,1.)+_pos,glm::vec2 (1.,-1.)+_pos,glm::vec4 (0.,1.,0.,1));

}
bool Projectile::tick ()
{

    _pos+=_vel;
    if (_pos.x>=512||_pos.x<0||_pos.y>=512||_pos.y<0)
    {
        return true;
    }
    else
    {
        std::list <Asteroids*>& asteroids=Asteroids::get_asteroids();
        std::list <Asteroids*>::iterator i=asteroids.begin();
        for (;i!=asteroids.end();i++)
        {

            Asteroids* a=*i;
            float dist=glm::length(a->getPos()-_pos);
            if (dist<a->getRadius())
            {
                a->doDamage(5.0);
                return true;
            }

        }
        return false;
    }

}
Projectile::Projectile(glm::vec2 pos,glm::vec2 vel)
{
    //ctor
    projectiles.push_back(this);
    _pos=pos;
    _vel=vel;
}

Projectile::~Projectile()
{
    //dtor
}
