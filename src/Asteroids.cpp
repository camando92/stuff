#include "Asteroids.h"
#include <list>
#include <cmath>
#include <cstdlib>



std::list<Asteroids*> asteroids;
void Asteroids::tick_all ()
{

    if (asteroids.empty ())
    {
      for (float theta=0;theta<2*M_PI;theta+=2.)
      {
          float r=128+64.*rand()/RAND_MAX;
          float radius=10+15.*rand ()/ RAND_MAX;
          new Asteroids (
            glm::vec2 (cosf (theta),sinf(theta))*r,
            glm::vec2 (-0.5+1.*rand()/RAND_MAX,-0.5+1.*rand()/RAND_MAX)*0.1f,
            radius
          );

      }

    }

    //printf ("%d\n",projectiles.size());
    std::list<Asteroids*>::iterator it=asteroids.begin();
    for (;it!=asteroids.end();)
    {
        Asteroids* ast=*it;
        bool dead=(ast)->tick ();
        if (dead)
        {
            it=asteroids.erase(it);
            delete ast;
        }
        else
        {
            it++;
        }


    }
}
void Asteroids::draw_all (Graphics* g)
{
    std::list<Asteroids*>::iterator it=asteroids.begin();
    for (;it!=asteroids.end();it++)
    {
        (*it)->draw (g);
    }
}
  std::list <Asteroids*>& Asteroids::get_asteroids ()
{
    return asteroids;
}
void Asteroids::doDamage (float damage)
{
    _health-=damage;

}
void Asteroids::draw (Graphics * g)
{
    //g->circle (_pos,_radius,glm::vec4 (0.,0.,1.,1.));
    for (int i=0;i<10;i++)
    {
        float theta=_rot+i*M_PI*2/10;
        float theta2=_rot+(i+1)*M_PI*2/10;
        glm::vec2 point=glm::vec2 (cosf(theta),sinf(theta))*_jags [i]+_pos;
        glm::vec2 point2=glm::vec2 (cosf(theta2),sinf(theta2))*_jags [(i+1)%10]+_pos;
        g->line(point,point2,glm::vec4 (0.,1.,1.,1.));
    }
}
bool Asteroids::tick ()
{
    _pos=_pos+_vel;
    _rot+=glm::length(_vel)*0.1;
    if (_pos.x>=512)
    {
        _pos.x-=512.f;
    }
    if (_pos.y>=512)
    {
        _pos.y-=512;
    }
    if (_pos.y<0)
    {
        _pos.y+=512;
    }
    if (_pos.x<0)
    {
        _pos.x+=512;
    }
   if (_health<0)
   {
       if (_radius>3)
       {
           for (int i=0;i<2;i++)
           {
               glm::vec2 vel=glm::vec2 (_vel.y,-_vel.x)*(i-0.5f)*4.f;
               new Asteroids(_pos,vel,_radius/2);
           }
       }

       return true;
   }
   else
   {
       return false;
   }
}
Asteroids::Asteroids(glm::vec2 pos,glm::vec2 vel,float radius)
{
    //ctor
     _rot=0;
    _pos=pos;
    _vel=vel;
    _radius=radius;
    _health=radius;
    for (int i=0;i<10;i++)
    {
        _jags [i]=_radius*(rand()*0.3/RAND_MAX+0.7);
    }
    asteroids.push_back(this);


}

Asteroids::~Asteroids()
{
    //dtor
}
