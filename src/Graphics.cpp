#include "Graphics.h"
#include <cmath>
int abs(int x) {
    if(x<0)
        x*=-1;
    return x;
}

void Graphics::line (glm::vec2 a,glm::vec2 b,glm::vec4 colour)
{
    int x0=a.x,x1=b.x,y0=a.y,y1=b.y;
    int dx=abs(x1-x0),dy=abs(y1-y0),error;
    int sx,sy;
    if (x0<x1)sx=1;else sx=-1;
    if (y0<y1)sy=1;else sy=-1;
    error=dx-dy;
    for (;;)
    {
        putpixel(glm::vec2 (x0*1.0f,y0*1.0f),colour);
        if (x0==x1&&y0==y1)break;
        int e2=error*2;

        if (e2>-dy)
        {
            error-=dy;
            x0+=sx;
        }
        if (e2<dx)
        {
            error+=dx;
            y0+=sy;
        }

    }

}
void Graphics::putpixel (glm::vec2 pos,glm::vec4 colour)
{
    int x=pos.x;
    int y=_height-pos.y;
    if (x>=0&&x<_width&&y>=0&&y<_height)
    {
        union
        {
            struct
            {
                 unsigned char b,g,r,a;
            };
            unsigned int bgra;
        }c;
        c.b=(unsigned char)(colour.b*255);
        c.g=(unsigned char)(colour.g*255);
        c.r=(unsigned char)(colour.r*255);
        c.a=(unsigned char)(colour.a*255);
        _pixels [y*_width+x]=c.bgra;

    }
}
void Graphics::circle (glm::vec2 pos,float radius,glm::vec4 colour)
{
    float y=0;
    float x=radius;
    float error= 1-x;
    while (x>=y)
    {
        putpixel(pos+glm::vec2 (x,y),colour);
        putpixel(pos+glm::vec2 (y,x),colour);
        putpixel(pos+glm::vec2 (-x,-y),colour);
        putpixel(pos+glm::vec2 (-y,-x),colour);
        putpixel(pos+glm::vec2 (-x,y),colour);
        putpixel(pos+glm::vec2 (x,-y),colour);
        putpixel(pos+glm::vec2 (-y,x),colour);
        putpixel(pos+glm::vec2 (y,-x),colour);
        y++;
        if (error<0)
        {
            error+=2*y+1;
        }
        else
        {
            x--;
            error+=2*(y-x+1);
        }
    }
}

Graphics::Graphics(unsigned int* pixels,int width,int height)
{
    //ctor

    _pixels=pixels;
    _width=width;
    _height=height;

}

Graphics::~Graphics()
{
    //dtor
}
