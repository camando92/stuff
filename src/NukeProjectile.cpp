#include "NukeProjectile.h"
#include <cstdlib>

NukeProjectile::NukeProjectile(glm::vec2 pos,glm::vec2 vel):Projectile ( pos,vel)
{
    //ctor
    _age=0;

}

NukeProjectile::~NukeProjectile()
{
    //dtor
}

void NukeProjectile::draw (Graphics* g)
{
    Projectile::draw (g);
}
bool NukeProjectile::tick ()
{
    _age++;
    if (_age>100&&rand ()<RAND_MAX/64)
    {
        for (int i=0;i<20;i++)
        {
            float theta=i*M_PI*2/20;
              glm::vec2 vel=glm::vec2 (cosf (theta),sinf (theta))*glm::length (_vel*0.5f)+_vel;
              new Projectile (_pos,vel);
        }
        return true;
    }
    else
    {
        return Projectile::tick ();
    }
}
