#ifndef ENGINE_H
#define ENGINE_H
#include "SDL/SDL.h"
#undef main
#include "Graphics.h"
#include "Ship.h"


class Engine
{
    public:

        void init ();
        void start ();
        Engine();
        ~Engine();
    private:
        Ship *_ship;
        void render ();
        bool tick ();
        Graphics* _graphics;
        int _width;
        int _height;
        SDL_Surface* _screen;
        unsigned int* _pixels;

        bool _keys [512];
        static const int NUM_KEYS=512;


};

#endif // ENGINE_H
