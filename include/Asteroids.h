#ifndef ASTEROIDS_H
#define ASTEROIDS_H
#include "Graphics.h"
#include <list>


class Asteroids
{
    public:

        static  std::list <Asteroids*>& get_asteroids ();
        static void tick_all ();
        static void draw_all (Graphics* g);
        glm::vec2 getPos ()
        {
            return _pos;
        }
        float getRadius ()
        {
            return _radius;
        }
        void doDamage (float damage);
        void draw (Graphics* g);
        bool tick ();
        Asteroids(glm::vec2 pos,glm::vec2 vel,float radius);


        virtual ~Asteroids();
    protected:
    private:
        float  _jags [10];
        glm::vec2 _pos, _vel;
        float _rot,_radius;
        float _health;
};

#endif // ASTEROIDS_H
