#ifndef GRAPHICS_H
#define GRAPHICS_H
#include <glm/glm.hpp>


class Graphics
{

    public:
        void circle (glm::vec2 pos,float radius,glm::vec4 colour);
        void line (glm::vec2 a,glm::vec2 b,glm::vec4 colour);
        Graphics(unsigned int* pixels,int width,int height);
        ~Graphics();
        void putpixel (glm::vec2 pos,glm::vec4 colour);

    protected:
    private:
        unsigned int* _pixels;
        int _width,_height;
};

#endif // GRAPHICS_H
