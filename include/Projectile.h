#ifndef PROJECTILE_H
#define PROJECTILE_H
#include "Graphics.h"


class Projectile
{
    public:
        static void tick_all ();
        static void draw_all (Graphics* grampa);
        virtual void draw (Graphics* gramma);
        virtual bool tick ();
        Projectile(glm::vec2 pos,glm::vec2 vel);
        virtual ~Projectile();
    protected:
         glm::vec2 _pos,_vel;
    private:

};

#endif // PROJECTILE_H
