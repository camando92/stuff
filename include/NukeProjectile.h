#ifndef NUKEPROJECTILE_H
#define NUKEPROJECTILE_H

#include <Projectile.h>


class NukeProjectile : public Projectile
{
    public:
        virtual void draw (Graphics* gramma);
        virtual bool tick ();
        NukeProjectile(glm::vec2 pos,glm::vec2 vel);
        virtual ~NukeProjectile();
    protected:
    private:
        float _age;
};

#endif // NUKEPROJECTILE_H
