#ifndef SHIP_H
#define SHIP_H
#include "Graphics.h"



class Ship
{
    public:

        void draw (Graphics* g);
        void tick (bool* keys);
        Ship();
        ~Ship();
    protected:
    private:
        int _health;
        int _hitTime;
        int _cooldown;
        glm::vec2 _vel;
        glm::vec2 _pos;
        float _rotation;
};

#endif // SHIP_H
